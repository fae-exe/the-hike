# **Pitch-Crafting**

- Draft a first set of intents
> Intent draft added to Vision Design as a list of pleasures to build around
- Brainstorm potential characters
> Done
- Brainstorm potential dangers
> Done
- Brainstorm potential quests
> Done
- Produce a first batch of pitch and ideas
- 