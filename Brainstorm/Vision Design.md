# Vision Design

## Inspirational experiential pleasures breakdown

### Why do people enjoy or want to play Celeste

- The pleasure of mastery, of understanding the controls and mastering them, then the pleasure and satisfaction of overcoming obstacles
- The pleasure of looking back and the feeling of progression from being able to take on ever tougher challenges
- The pleasure of intrinsic motivation, of pushing forward, of feeling determined to accomplish something very difficult for its own sake
- The emotional resonance with Celeste and her struggles
- The immersion in the struggle against the mountain, the emotional investment
- The pleasure and emotional resonance of the journey up the mountain and the way it resonates with Celeste's personal struggle
- The pleasure of efforts paying-off, of gradual slow achievement
- The pleasure of the juice and the game-feel of the controls, of getting into the flow of navigating the environment with more and more ease
- The pleasure and satisfaction of the self-expression offered by the controls and the ways to traverse the environment
- The pleasure of discovery in exploring the environment and finding hidden strawberries and secrets and of seeing landscape so hard to reach that they become precious
- The pleasure and the thrill of overcoming very difficult sections where the elements are sometimes against you
- The pleasure of the evoked escapism and introspection from the mountainous setting which resonates with the idea of mountain climbing
- The pleasure of the implicit connexion with other people playing Celeste and trying to climb the same mountain and understanding the same struggle; the pleasure of connexion to others through shared experience

### Why do people enjoy or want to play Animal Crossing

- The pleasure of self-expression through landscaping, clothes, villager choices, every decisions the game allows for
- The pleasure of anticipation and pay-off, through the patience required by the pacing of the game, doing something then waiting for it to come to fruition
- The pleasure of novelty and discovery, every day new things happening and creating this want to come back and see what is new
- The pleasure of collecting (and self-expression through collecting) through collecting clothes, furnitures, bugs and all the collectibles
- The pleasure of curating (and self-expression through it), for selecting a list of favorite "dream" villagers, then selecting one's favorite villagers and slowly gathering them for one's town
- The pleasure of slow gradual accomplishment and looking back on one's own progress (sense of progression reflected in the village's state and villager's house)
- The pleasure and satisfaction and cleaning and organizing and of reviewing the result of one's own work
- The pleasure and need for escapism into a life free of the trappings of our own daily life (or set in very different aesthetic conventions)
- The pleasure of crafting, of a work and reward cycle that makes everything feel earned and builds up emotional investment and connexion to one's own possessions
- The pleasure of acting upon the world by one's own means, of having an impact and making a difference (the pleasure of meaningful action?) using tools one has built up to by oneself

### Why do people enjoy or want to play Journey

- The pleasure of curiosity and discovery, of discovering new landscapes and new secrets hidden in the scenery, of learning about the stories through the frescoes in the ruins and trying to piece together an idea of what happened
- The visual pleasure of the picturesque landscapes and the evocative power of the still images and of the sceneries (also as a backdrop for the pleasure of connexion/shared experience)
- The pleasure of the reactivity of the world, the feeling that it's a living world, and of being part of it
- The pleasure of whimsy and wonder with the sparks and sparkles and discovering enchanting locales and seeing events happening that feel special and rare and foster that feeling of whimsy that comes with being part of something special
- The pleasure of immersion, of feeling part of that world and letting everything fade away, the pleasure and need for escapism that comes with it
- The pleasure of the freedom and effortlessness of the controls and of the acquired mastery of them which makes them feel like part of you and help with the immersion
- The pleasure of flying and the exhilaration that comes from it
- The sense of progression and the pleasure of the journey, having a goal and making progress, step by step, towards it, and seeing tangible traces of that progress, with the length of the scarf for instance and the fact that you can then fly farther.
- The feeling of connexion, humanity and resonance through shared experience coming from travelling with an other player
- The pleasure of self-expression within constraints, using the limited means of communication and the means of movement to still express oneself (which creates a public to witness that self-expression, which makes it more significant)

### Why do people enjoy and resonate with the idea of climbing a mountain

- The pleasure of emotional resonance with the idea of overcoming something that's far greater than oneself, even though one is very small and weak
- The pleasure of the thrill of danger and defying something powerful and putting one's life on the line
- The pleasure of the gradual, tangible progression linked to the verticality of the climb, and the literal ability to look back upon one's progression and see how far one has come.
- The pleasure and the emotional resonance of going literally outside of one's comfort zone and overcoming one's limits, of going beyond oneself
- The pleasure of discovery in seeing new landscapes and new places one would have never seen otherwise
- The pleasure of the rarity and exclusivity of the experience: since it's very hard, going this far makes it special and makes one feel special and makes the memories precious
- The need to prove to oneself or to others that one is able to triumph over something as daunting as climbing a mountain
- The pleasure or the need for escapism, in fleeing something from one's own life into a place that's very isolated and far from civilisation
- The pleasure of the change in scenery, of being exposed to a space that's very different from what one is used to, that changes one's habits and pushes towards renewing oneself, finding a breathe of fresh air
- The pleasure and the need for introspection, for which the isolation, the change in scenery, and the monotony of the landscape make a favorable context

### Why do people enjoy and resonate with the idea of survival in a snow and mountain wildlife environment

- The pleasure of immersion in a different environment, cut from the rest of the world, like suspended in a bubble, fully part of something different and greater than oneself
- The pleasure of escapism, of being cut from the usual day to day life, of being able to run away from it and hide someplace else
- The pleasure of control, of being the master of one's own fate, of reclaiming that control and keeping it against strong forces that seek to take it away
- The pleasure of survival, of overcoming the dangers of the environment and taming it
- The pleasure of harmony and peacefulness, of feeling like part of something bigger, of finding an outward peace that brings an inward peace.
- The pleasure of discovery and change in scenery, like a breath of fresh air, being in something different than one's usual life, of finding new things all the time, the pleasure of being surprised, of being able to satisfy curisities and wonder and seeing things with new eyes.
- The pleasure of acclimmatation, of something that's fearful and dangerous becoming familiar and reassuring, of making a wild place feel like home.
- The pleasure of whimsy and wonder, through seeing beautiful things, be them sceneries or witnessing emergent events, or eliciting because of one's own action or presence events bearing whimsy.
- The pleasure of exclusivity, because of the loneliness or the hard-to-reach character of the place, of being one of the few people able to witness this place
- The pleasure of connexion and shared experience, both through sharing memories and recollections and pictures with other people, but also through time with the other persons who have been present in this place, through seeing their traces and feeling empathy towards them and imagining how their journey mirrored yours
- The pleasure of self-expression, in the way that one tackles the problematic of survival and engages with the nature around, in the way one organizes their gear and environment and navigates through space, and in the way one chooses to immortalize the experience (text, pictures...)
- The pleasure of navigating a complex decision space, of learning to understand how the environment works and make decisions based on the way that environment functions
- The pleasure of planning and execution; the pleasure of planning a path, the way to traverse that route, then executing on it, or the pleasure of planning the actions necessary for survival, then successfully executing on it.
- The visual pleasure of the environment and the context enhancing that pleasure, the pleasure of capturing the view of the scenery at a specific moment (linked to the pleasure of witnessing, tied into whimsy and wonder)
- The pleasure of witnessing emergent events, the pleasure of being there at just the right moment to see something special happen, and of being able to recount and relive that moment and share it with others, while feeling its preciousness because of its rarity and exclusivity

## First Intentionality Draft

- The pleasure of emotional resonance and empathy
- The pleasure of efforts paying-off, of gradual slow achievement
- The pleasure of curiosity and discovery
- The pleasure of the connexion through shared experience and implicit connexion
- The pleasure of collecting (and self-expression through collecting)
- The pleasure of looking back on one's own progress
- The pleasure of meaningful action
- The pleasure of the reactivity, feedback and the living world
- The pleasure of immersion
- The pleasure and the emotional resonance of going literally outside of one's comfort zone and overcoming one's limits, of going beyond oneself
- The pleasure of the rarity and exclusivity of the experience
- The pleasure or the need for escapism
- The pleasure of the change in scenery
- The pleasure of whimsy and wonder
- The pleasure of decision-making
- The pleasure of witnessing
- The pleasure of novelty and discovery
- The pleasure of freedom and effortlessness
- The pleasure of a gradual, tangible progression
- The pleasure and the need for introspection
- The pleasure of harmony and peacefulness, of feeling like part of something bigger, of finding an outward peace that brings an inward peace.
- The pleasure of acclimmatation, of something that's fearful and dangerous becoming familiar and reassuring, of making a wild place feel like home.
- The pleasure of planning and execution

## Starting from the pitch

> Une fée de la lune, élevée par la déesse de la nuit - une divinité gardienne de la forêt à forme de chouette - dans une cabane au bord d'un lac, va partir en expédition afin de sauver le printemps du grand dévoreur.
> Elle traversera une forêt primordiale, et partira à l'ascension du pic des origines. Sur sa route, elle rencontrera des divinités de la montagne, les peuples des feuilles qu'elle devra convaincre de l'aider, et des esprits célestes associés aux saisons. Elle devra récolter des graines et des plantes magique afin de rétablir l'ordre des saisons et de la vie, et se recueillir dans les sanctuaires sur sa route, pour obtenir la bénédiction des dieux et des esprits.
> Pour cela, elle devra utiliser les pouvoirs de la plume de lumière, confiée par la chouette, des ciseaux d'herboriste contenant les pouvoirs du printemps, ainsi que la lanterne du gel qui lui confère un pouvoir sur le froid.

How might we capture these pleasure through this experience

- The pleasure of emotional resonance and empathy
  - Feedbacks and particle effects on characters to show their emotions
  - Engage in tiny quests/interactions with the characters that lead to shows of emotions
  - Have the nature around react to these emotions
  - Allow the player to emote with their character at any moment and have some form of impact on the close environment (tiny visual feedback elements like vfx, colors, tiny animations)
  - Characters react to environmental input with tiny emotes (something falling -> startled emotes)
  - Characters reacting to seeing the player in various ways depending on how the relationship with the player has been going (internal values tied to behaviours? If they don't like the player, they run away to hide or shiver, if they like the player they cheer, wave, heart...)
  - Possibility to pet/feed animals and have them follow you
  - Mirroring - characters replicating your emotes at you when you emote at them in one way?

- The pleasure of efforts paying-off, of gradual slow achievement
  - Start with a very wintery environment, gradually bringing back the seasons
  - Show change in the environment in line with the player's actions
  - A garden or the cabin growing prettier, with accumulated stuff and souvenirs
  - Elements of the player character evolving (wings growing, antlers groing, symbols glowing - the progress being reflected in the character model)
  - Plants growing over playtime and giving rewards regularly or allowing you to cross some gap...
  - L'apparence de la carte qui change avec le temps
  - Ressuciter/réparer les autels des esprits a un impact sur la zone/sur la carte et donne un souvenir pour la cabine
  - The inhabitant's emotions towards you slowly changing
  - People moving into your cabin and building it out and having their dialogue slowly changing throughout the game

- The pleasure of curiosity and discovery
  - Nonlinear map with future areas being visible but clearly not yet accessible
  - New dialogues regularly fueling the want to explore and discover
  - Room for experimentation in the mechanics: finding new ways to use a mechanic
  - Slowly giving access to new powers or new types of tools (types of seeds?)
  - Collecting bits of a story that unfolds as you move forward in the story
  - Eliciting curiosity through narrative and scenario, unfolding stories about spirits, inhabitants, deities as you go and tying them to your actions
  - Hinting at possible things (possible upgrades/actions/transformation/locations) while only giving snippets of information to the player

- The pleasure of the connexion through shared experience and implicit connexion
  - Journals of someone who went through the same hardships as we did
  - Messages from other players left in various places
  - Elements from one specific other player's playthrough throughout our game
  - Being able to take or paint pictures of the scenery and share them or keep them
  - Tales from other people who went through the same thing
  - Tiny scenes telling implicit stories with which the player/character might resonate
  - Traces or remainders of emoting in specific places

- The pleasure of collecting (and self-expression through collecting)
  - Collecting plants and herbs for your journal
  - Collecting pictures or paintings. Maybe some of them are of specific places or landscapes or viewpoints, and you can sell them or trade them?
  - Journal pages from some other explorers or people
  - Tales or legends from the divinities
  - Furnitures and decorations for the cabin
  - Tokens of gratitude from the inhabitants
  - Bugs from the forest?
  - Tiny mementos from various places?
  - Inhabitants coming to live in the cabin?
  - Collecting plants and seeds to then use elsewhere

- The pleasure of looking back on one's own progress
  - A journal to consult as you go through your adventure
  - Being able to hang paintings in the cabin, move the furniture
  - The thing that you've collected
  - Write on the world map and tie that to parts of the journal
  - Reminders of how far you've come and what you still have to do: visual representations of the activated shrines?

- The pleasure of meaningful action
  - Persistent impactful changes on the environment - planting a seed will grow a plant will grow a tree
  - Changes in the environment stay for good and change the landscape and the way to navigate it
  - Decisions have consequences in the way the inhabitants emote at you and treat you
  - Actions have consequences in other part of the world to make it feel interconnected
  - Actions have persistent consequences but the non-scripted ones must be reversible (freezing some water -> must be able to thaw it)

- The pleasure of the reactivity, feedback and the living world
  - Make things react to the presence of the character

- The pleasure of whimsy and wonder
  - Seed scripted events that might evoke whimsy and wonder as they are discovered

- The pleasure of freedom and effortlessness
  - Create ways to travel the environment that feel effortless

- The pleasure and the need for introspection
  - Give way to lows in the action that allow for this introspection
  - Times around the campfire with nice music and maybe some dialogue
  - Day-night cycle with the nightfall being marked by some quiet and introspective dialogue
  - Mark moments in the action that are specifically reserved to create that feeling
  - Create sceneries with scripted camera movement/rails to show the landscape, make it cinematic, with music and narration
  - Add narration to the game to narrate the character's actions

- The pleasure of harmony and peacefulness, of feeling like part of something bigger, of finding an outward peace that brings an inward peace.
  - Allow the character to mediatate or become in touch/attuned to the environment
  - Propagate the inward peace to the outward environment to communicate its presence
  - Reflect the characters inner turmoil in the environment/make some conflict about emotions of the character

- The pleasure of acclimmatation, of something that's fearful and dangerous becoming familiar and reassuring, of making a wild place feel like home.
  - Highlight paths or interaction the character has taken enough time
  - Make the fauna in an environment like the character after she has repaired the place or give her a way to interact with them
  - A language that the players/character can learn slowly that will make places more familiar as it goes
  - Slowly make the environment easier to navigate

> Travel through the mountain and explore it

> Help the inhabitants and the spirits

> Restoring the seasons and the sacred places